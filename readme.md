***********************************************************************
***                                                                 ***
***                            RAYTRACER                            ***
***                                                                 ***
***********************************************************************
ReadMe

What is Ray Tracing?
Raytracing is a rendering technique which can be used to produce 
effective lightning effects. We can generate an image of any 3D object by
tracing the rays coming from a light source or an eye going through objects.
If we follow the path of the photon forward from the light source to the
observer to render the image,it is called forward ray tracing. Since it's
hard to know how much amount of photon must be emmitted to get a proper
image of the object, this is wasteful.
Instead of tracing rays from the light source through the receptor, we
can follow the rays from the receptor to the objects. This is called back
ray tracing.
Image is to be colorized depending on the hit point, light and the surface
normal n.

To write a basic Raytracer:
{

	for each pixel do

		compute viewing ray

		first object hit by ray and it's surface normal n

		set pixel color to value computed from hit point,light and n
		
}

My RayTracer

My project aims to write a basic Raytracer which is able to render spheres
and a plane. Restrictions are listed below:

--> Only one light source is allowed. The light source which is set last is 
to override the older source.

--> Spheres and a plane can be rendered. Triangle is not supported.

--> Setting camera position is not supported. It's to be implemented.

How does it Work?

It  simply computes the primary camera rays for each single pixel of the 
image plane and traces the rays passing through this pixels through the 
objects.By doing that, we can easily calculate the hit point. According to
the position, brightness and color of the light source, it calculates the
color for each hit point.

The scene which is hard-coded in main.cpp is defined by three spheres and a
plane lying on -z axis. Once you run the program, it'll generate an image 
of that scene named "scene.ppm".

Some sample outputs are already added here,please check scene1.ppm,scene2.ppm,
scene3.ppm, scene4.ppm, scene5.ppm.


To run the program by yourself, please follow the steps below:
 
To make the project:
*** Please run "make" on the  console.

To run the program and generate the scene.ppm
*** Write basically "raytracer" to the console.

To clear the output files
*** Please run "make clean" on the  console.

Had the need arise, please feel free to ask: simgesonmez90@gmail.com.

