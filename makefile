appname := raytracer

CXX := g++
CXXFLAGS := -g -std=c++17

INCLUDE := -Iutilities -Iscene 
SRCFILES := \
    scene/Sphere.cpp \
    scene/Plane.cpp \
    scene/Light.cpp \
    Raytracer.cpp \
    main.cpp

all: $(SRCFILES)
	$(CXX) $(CXXFLAGS) -o raytracer $(INCLUDE) $(SRCFILES)

clean: 
	$(RM) raytracer scene.ppm


