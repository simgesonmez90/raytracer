/**
 * @file   Sphere.hpp
 * @brief  Sphere class implements a sphere which is defined by
 *         a point, a radius and a color.
 */

#ifndef _SPHERE_
#define _SPHERE_

#include "AnyObject.hpp"
namespace scene
{
class Sphere : public AnyObject
{
public:
    Sphere();
    Sphere(int const& radius,
           Point const& center, 
           Color const& color);
    virtual ~Sphere() = default;

    /* Implements AnyObject */
    Color getColor() const override;

    bool intersect(Point const &rayOrig,
                   Ray const &rayDir, 
                   float &t0,
                   float &t1) const override;

    bool intersectPoint(Point const &rayOrig,
                        Ray const &rayDir, 
                        Point &point) const override;

    void getIntersectionNormal(Point const &point, 
                               Ray& normal) const override;

private:
    int m_radius;
    Point m_center;
    Color m_color;

    static const int default_radius;
    static const Point default_point;
    static const Color default_color;
};
}
#endif //_SPHERE_
