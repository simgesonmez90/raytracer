/**
 * @file   Plane.hpp
 * @brief  Plane class implements a plane which is defined by
 *         a point, a normal vector and a color.
 */

#ifndef _PLANE_
#define _PLANE_

#include "AnyObject.hpp"

static const Color ref_color_plane(255,0,0);
namespace scene
{
class Plane : public AnyObject
{
public:
    Plane();
    Plane(Point const &point,
          Ray const &normal, 
          Color const &color);
    virtual ~Plane() = default;

    /* Implements AnyObject */
    Color getColor() const override;

    bool intersect(Point const &rayOrig,
                   Ray const &rayDir, 
                   float &t0,
                   float &t1) const override;

    bool intersectPoint(Point const &rayOrig,
                        Ray const &rayDir,
                        Point &point) const override;

    void getIntersectionNormal(Point const &point, 
                               Ray& normal) const override;

private:
    Point m_position;
    Ray   m_normal;
    Color m_color;

    static const Color default_color;
    static const Ray default_normal;
    static const Point default_position;
};
}
#endif // _PLANE_
