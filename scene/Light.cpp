/**
 * @file   Light.cpp
 * @brief  
 */

#include "Light.hpp"

const Color scene::Light::default_color = Color(255,255,255);
const Point scene::Light::default_position = Point(-10,10,-5);

namespace scene
{
Light::Light()
    : m_position(default_position)
    , m_color(default_color)
{
}

Light::Light(Point const &position, 
             Color const &color)
    : m_position(position)
    , m_color(color)
{
}

Point 
Light::getPosition() const
{
    return m_position;
}

Color 
Light::getColor() const
{
    return m_color;
}
}
