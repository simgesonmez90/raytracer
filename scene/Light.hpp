/**
 * @file   Light.hpp
 * @brief  Light class implements a plane light is
 *         defined by a point and a color.
 */

#ifndef _LIGHT_
#define _LIGHT_

#include "AnyObject.hpp"
namespace scene
{
class Light
{
public:
    Light();
    Light(Point const &position, Color const &color);

    ~Light() = default;

    /** 
     * @brief Get the position of the light,default
     *        position is Point(-10,10,-5) unless it
     *         is defined in construction.
     * @return the position of the light.
     */
    Point getPosition() const;

    /** 
     * @brief Get the color of the light defined by
     *        R,G and B, the default color is white
     *        (Color(255,255,255)) unless it is defined
     *        in construction.
     * @return the color of the light.
     */
    Color getColor() const;

private:
    /*delete copy constructor and assignment operator*/
    Light(Light const& /*light*/) = delete;
    Light& operator=(Light const& /*light*/) = delete;

private:
    Point m_position;
    Color m_color;

    static const Color default_color;
    static const Point default_position;
};
}
typedef std::unique_ptr<scene::Light> LightPtr;
#endif //_LIGHT_
