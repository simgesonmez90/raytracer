/**
 * @file   Sphere.cpp
 */

#include "Sphere.hpp"
#include <math.h>

const int scene::Sphere::default_radius(5);
const Point scene::Sphere::default_point(0,0,0);
const Color scene::Sphere::default_color(0,255,0);
namespace scene
{
Sphere::Sphere() 
       : m_radius(default_radius)
       , m_center(default_point)
       , m_color(default_color)
{
}

Sphere::Sphere(int const& radius,
               Point const& center,
               Color const& color) 
       : m_radius(radius)
       , m_center(center)
       , m_color(color)
{
}

Color
Sphere::getColor() const
{
    return m_color;
}

bool
Sphere::intersect(Point const &rayOrig,
                  Ray const &rayDir, 
                  float &t0,
                  float &t1) const
{
    /* This is how to calculate sphere-ray intersections:
     * Find the length between sphere-center and the ray 
     * origin.
     * There is a triangle here which has three sides namely,
     * length, tca and d.
     * There is another rectangle inside the sphere which has
     * three sides namely, thc, d and radius.
     * (thc+tca) and (tca-thc) values give us the intersection
     * points.
     */
    Ray length = Ray(m_center - rayOrig);
    float tca = length.dotProduct(rayDir);
    if (tca < 0)
    {
        return false;
    }

    float d2 = length.dotProduct(length) - tca*tca; 
    if (d2 > (m_radius*m_radius))
    {    
        return false;
    }
    float thc = sqrtf((m_radius*m_radius) - d2);
    t0 = tca - thc;                                       
    t1 = thc + tca;
    return true;
}

bool 
Sphere::intersectPoint(Point const &rayOrig,
                       Ray const &rayDir,
                       Point &point)  const
{
    float t0 = 0; 
    float t1 = 0;
    if (intersect(rayOrig, rayDir, t0, t1))
    {
        point = (rayOrig + rayDir*t0);
        return true;
    }
    return false;
}

void 
Sphere::getIntersectionNormal(Point const &point, 
                              Ray& normal) const
{
    normal = (point - m_center).normalize();
}
}
