/**
 * @file   AnyObject.hpp
 * @brief  
 */

#ifndef _ANYOBJECT_
#define _ANYOBJECT_

#include <memory>
#include "Vector.inl"
/**
 * Ray, Point and Color are defined by three values.
 * Ray and Point are expected to be defined by x,y and z values,
 * As Color is defined by r,g and b values. 
 */
typedef Vect<float> Ray;
typedef Vect<float> Point;
typedef Vect<unsigned char> Color;
namespace scene
{
class AnyObject
{
public:
    virtual ~AnyObject() = default;

    /** 
     * @brief Gets the color info of the object.
     * @return returns the color of the object.
     */
    virtual Color getColor() const = 0;

    /** 
     * @brief Checks if any ray defined by the origin and the direction 
     *        intersects the object or not.
     * @param rayOrig the origin point of the ray coming
     * @param rayDir the direction of the ray coming
     * @param t0 to be populated with the distance between the ray origin
     *        and the first intersection point
     * @param t0 to be populated with the distance between the ray origin
     *        and the second possible intersection point
     * @return true,if the ray intersects the object, false, otherwise.
     */
    virtual bool intersect(Point const &rayOrig,
                           Ray const &rayDir, 
                           float &t0,
                           float &t1) const = 0;

    /** 
     * @brief Gets the intersection point
     * @param rayOrig the origin position of the ray coming
     * @param rayDir the direction of the ray coming 
     * @param point to be populated with the position of the light source
     * @return true,if the ray intersects the object and the intersection
     *         point is successfully obtained, false, otherwise.
     */
    virtual bool intersectPoint(Point const &rayOrig,
                                Ray const &rayDir,
                                Point &point) const = 0;
    /** 
     * @brief Get the normal vector passing through the point intersected
     *        by the ray.
     * @param point the intersection point
     * @param normal to be populated with the normal vector.
     */
    virtual void getIntersectionNormal(Point const &point,
                                       Ray &normal) const = 0;
};
}
typedef std::unique_ptr<scene::AnyObject> AnyObjectPtr;
#endif //_ANYOBJECT_
