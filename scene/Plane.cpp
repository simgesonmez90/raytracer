/**
 * @file   Plane.cpp
 */

#include "Plane.hpp"

// Default value of a plane color is green,
// Default value of a plane position is (0,-20,0),
// Default value of a plane normal is (0,0,1)
const Point scene::Plane::default_position = Point(0,-20,0);
const Ray scene::Plane::default_normal     = Ray(0,0,1);
const Color scene::Plane::default_color    = Color(0,0,255);

namespace scene
{
Plane::Plane()
      : m_position(default_position)
      , m_normal(default_normal)
      , m_color(default_color)
{
}

Plane::Plane(Point const &point,
             Ray const &normal,
             Color const &color) 
             : m_position(point)
             , m_normal(normal)
             , m_color(color) 
{
}

Color
Plane::getColor() const 
{
    return m_color;
}

bool 
Plane::intersect(Point const &rayOrig,
                 Ray const &rayDir, 
                 float &t0,
                 float &t1) const
{        
    /* Ray = RayOrig + rayDir.t
     * Plane = (Ray - Position).dot(normal)
     * Plane = (RayOrig + rayDir.t - Position).dot(normal)
     * 0 = RayOrig.dot(normal) + rayDir.t.dot(normal) - Position.dot(normal) 
     * rayDir.t.dot(normal) =Position.dot(normal) - RayOrig.dot(normal)
     * t = (Position - RayOrig).dot(normal) / rayDir.dot(normal)
     */
    float dDotN = rayDir.dotProduct(m_normal);
    if (dDotN == 0.0f)
    {
        return false;
    }

    Ray ray = m_position - rayOrig;
    t0 = ray.dotProduct(m_normal) / dDotN;
    if (t0 >= 0)
    {
        return true;
    }
    return false;     
}

bool 
Plane::intersectPoint(Point const &rayOrig,
                      Ray const &rayDir,
                      Point &point)  const
{
    float t0 = 0, t1 = 0;
    if (intersect(rayOrig, rayDir, t0, t1))
    {
        point = (rayOrig + rayDir*t0);
        return true;
    }
    return false;
}

void
Plane::getIntersectionNormal(Point const &point,
                             Ray& normal) const
{
    normal = m_normal;
}
}
