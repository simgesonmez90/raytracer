/* 
 * @brief 
 */

#include "Raytracer.hpp"

#include <math.h>
#include <fstream>
#include <iostream>
#include <algorithm>

#define INFINITE 1000000

void
Raytracer::defineSizeOfRenderingCanvas(unsigned int width,
                                       unsigned int height)
{
    m_width = width;
    m_height = height;
}

void
Raytracer::setSceneLight(LightPtr& light)
{
    m_light = std::move(light);
}

void
Raytracer::addObject(AnyObjectPtr& object)
{
    m_objects.push_back(std::move(object));
}

void 
Raytracer::defineScene(std::vector<AnyObjectPtr>& objects)
{
    for (auto& object : objects)
    {
        m_objects.push_back(std::move(object));
    }
}

void 
Raytracer::setCameraPosition(Point const &position, Ray const& up_vector)
{
    //TODO: setting camera position will be implemented.
}

void
Raytracer::render()
{
    float aspectRatio = m_width / (float)m_height;
    Point rayOrigin(0,0,0);

    std::ofstream ofs;
    ofs.open("scene.ppm", std::ofstream::out | std::ofstream::binary);
    ofs << "P6\n" << m_width << " " << m_height << " " << "\n255\n";
  
    for (int i=0; i<m_height; ++i)
    {
        for (int j=0; j<m_width; ++j)
        {
            /**
             * To compute the camera rays, we need to scale the image plane 
             * to unit plane  first. Then we should multiply it with 2 and
             * shift one unit to right and one unit to down. The result gives
             * us the position of each pixel.
             * As i chose to operate on -z axis, my camera rays will point to 
             * -z direction.
             */
            float pixel_x = (2*((j + 0.5)/float(m_width)) - 1)*aspectRatio;
            float pixel_y = (-2*((i + 0.5)/float(m_height)) + 1);
            Point cameraSpacePixel(pixel_x, pixel_y, -1);

            Ray rayDir = Ray(cameraSpacePixel - rayOrigin).normalize();  
            Color pixel = trace(cameraSpacePixel, rayDir);
            ofs << pixel.x << pixel.y << pixel.z;
         }
    }
    ofs.close();
}

Color
Raytracer::trace(Point const& rayOrigin, Ray const& rayDir) const
{
    float t0 = 0.0;
    float t1 = 0.0;
    Color pixel(0,0,0);

    scene::AnyObject* my_object = nullptr;
    float minDistance = INFINITE;  

    // find the first object hit by the ray
    for (int k = 0; k < m_objects.size(); ++k)
    { 
        if (m_objects.at(k)->intersect(rayOrigin, rayDir, t0, t1)) 
        {
            if (t0 < minDistance)
            {
                my_object = m_objects.at(k).get();
                minDistance = t0;
            }
        }
    }

    //if the ray hits any object in the scene, check if it's in shadow
    bool isInShadow = false; 
    if (my_object != nullptr) 
    { 
        Point point = (rayOrigin+rayDir*minDistance);
        Ray shadowRay = (m_light->getPosition() - point);
        Ray shadowDir = shadowRay.normalize();
        for (int k = 0; k < m_objects.size(); ++k)
        { 
            if (m_objects.at(k)->intersect(rayDir*minDistance, shadowDir, t0, t1)) 
            { 
                isInShadow = true; 
                break; 
            } 
        }  
  
        // Check if it's in the shadow
        if (!isInShadow)
        {
            Ray normal;
            Point intersect_point;
            if (my_object->intersectPoint(rayOrigin, rayDir, intersect_point))
            {
                 my_object->getIntersectionNormal(intersect_point, normal);
                 Ray lightDir = (m_light->getPosition() - intersect_point).normalize();

                 Color object_col = my_object->getColor();
                 Color light_col = m_light->getColor();

                 pixel = light_col*0.5*std::max((float)0, normal.dotProduct(lightDir));
                 pixel = pixel + object_col*0.2;
            }
        }
        else
        {
            pixel = pixel + Color(2,2,2);
        }
    }
    return pixel;
}

