#include <math.h>

template <typename Type>
class Vect
{
public:
    Vect() = default;

    Vect(Type val)
    : x(val)
    , y(val)
    , z(val) {};

   Vect(const Vect<Type>& vec)
    : x(vec.x)
    , y(vec.y)
    , z(vec.z) {};

    Vect(Type valX, Type valY, Type valZ) 
    : x(valX)
    , y(valY)
    , z(valZ) {};

    ~Vect() = default;

    Vect<Type> operator-(Vect<Type> vec) const
    {
        return Vect<Type>(x-vec.x, y-vec.y, z-vec.z);
    };

    Vect<Type> operator+(Vect<Type> vec) const
    {
        return Vect<Type>(x+vec.x, y+vec.y, z+vec.z);
    };

    Vect<Type> operator*(float t) const
    {
        return Vect<Type>(t*x, t*y, t*z);
    };

    bool operator==(Vect<Type> vec) const
    {
        return (x == vec.x && y == vec.y && z == vec.z);
    };

    Vect<Type> normalize() const
    {
        if ((x*x + y*y + z*z) > 0) {
            Type length = sqrtf(x*x + y*y + z*z);
            return Vect<Type>(x/length, y/length, z/length);
        }
    };

    Type dotProduct(Vect<Type> vector) const
    {
        return (x*vector.x + y*vector.y + z*vector.z);
    };

    void crossProduct(Vect<Type> vector, Vect<Type>& result) const
    {
        result.x = y*vector.z - z*vector.y;
        result.y = - x*vector.z + z*vector.x;
        result.z = x*vector.y - y*vector.x;
    };

    Type length() const
    {
        return sqrtf(x*x + y*y + z*z);
    };

    Type x;
    Type y;
    Type z;
};

