/* 
 * @brief 
 */

#ifndef _RAYTRACER_
#define _RAYTRACER_
#include <vector>

#include "AnyObject.hpp"
#include "Light.hpp"

class Raytracer
{
public:
    Raytracer() = default;
    ~Raytracer() = default;

    /** 
     * @brief Define the size of rendering canvas
     * @param width width of the rendering canvas
     * @param height height of the rendering canvas
     */
    void defineSizeOfRenderingCanvas(unsigned int width,
                                     unsigned int height);

    /** 
     * @brief Sets the unique light source
     * @return light light source defined by position and 
     *         color. As we have only one source, new light
     *         is to override the old.
     */
    void setSceneLight(LightPtr& light);

    /** 
     * @brief Define scene using objects. Each object is defined
     *        by position and color.
     * @return objects vector of objects
     */
    void defineScene(std::vector<AnyObjectPtr>& objects);

    /** 
     * @brief Adds an object to the scene
     * @return object to be added.
     */
    void addObject(AnyObjectPtr& object);

    /** 
     * @brief Sets camera position.
     * @param position position of the camera
     * @param up_vector the unit vector pointing to the sky.
     */
    void setCameraPosition(Point const &position, Ray const& up_vector);

    /** 
     * @brief Renders the scene
     */
    void render();
private:
    /* Forbidden to copy and assign */
    Raytracer(const Raytracer& tracer) = delete;
    Raytracer& operator=(const Raytracer& tracer) = delete;

   Color trace(Point const& rayOrigin, Ray const& rayDir) const;

private:
    unsigned int m_width;
    unsigned int m_height;

    LightPtr m_light;
    std::vector<AnyObjectPtr> m_objects;
    
};
#endif   // _RAYTRACER_

