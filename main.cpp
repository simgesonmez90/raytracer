#include "Raytracer.hpp"
#include "Sphere.hpp"
#include "Plane.hpp"

#define sphereSize 5

int main(void)
{
    Raytracer raytracer;
    raytracer.defineSizeOfRenderingCanvas(1280, 720);  

    /* Create light defined by position and color */
    LightPtr light(new scene::Light(Point(20, 20,-15),Color(255,255,255)));
    raytracer.setSceneLight(light);

    /* Create scene objects defined by their position and color*/
    AnyObjectPtr sphere(
        new scene::Sphere(sphereSize,Point(20,-5,-43),Color(255,0,0)));

    AnyObjectPtr sphere2(
       new scene::Sphere(sphereSize,Point(-3,-5,-43),Color(0,255,0)));

    AnyObjectPtr sphere3(
       new scene::Sphere(sphereSize,Point(10,-5,-43),Color(0,0,255)));

    AnyObjectPtr plane(
       new scene::Plane(Point(-17,-17,0),Ray(1,1,0),Color(255,255,0)));

    std::vector<std::unique_ptr<scene::AnyObject>> objects;
    objects.push_back(std::move(sphere));
    objects.push_back(std::move(sphere2));
    objects.push_back(std::move(sphere3));
    objects.push_back(std::move(plane));

    /* Set scene, place light and the objects */
    raytracer.defineScene(objects);

    raytracer.render();
    return 0;
};
